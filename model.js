const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//{type: String, default: ""}
/*
UserSchema.method('update', function(updates, callback){
  Object.assign(this, updates, { updatedAt: new Date() });
  this.parent().save(callback);
});

//old pass? code sent by email?
UserSchema.method('changePassword', function(oldPassword, newPassword, callback){
  if(this.password === oldPassword){
    this.password = newPassWord
  }
  this.parent().save(callback);
});
*/
const UserSchema = new Schema({
  name: String,
    email: String,
    password: String,
    //userID: String,
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  contacts: {type: Array, default: []},//ids
  chats:  {type: Array, default: []}//chatID
}, { strict: false });

UserSchema.method('update', function(updates, callback) {
  Object.assign(this, updates, { updatedAt: new Date() });
  this.save(callback);
});




//change password

const ChatSchema = new Schema({
    chatID: String, //or name?
    users: Array, 
    messages: Array,
    createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
}, { strict: false });
/*
ChatSchema.method('update', function(updates, callback){
  Object.assign(this, updates, { updatedAt: new Date() });
  this.parent().save(callback);
});
*/
const MessageSchema = new Schema({
    userID: String, //
  text: String,
  chatID: String,
    //what about photos or docs?
    createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
}, { strict: false });
//add to chat
//uploadPhoto?
/*
MessageSchema.method('update', function(updates, callback){
  Object.assign(this, updates, { updatedAt: new Date() });
  this.parent().save(callback);
});

*/


const User = mongoose.model('User', UserSchema);
//const Chat = mongoose.model('Chat', ChatSchema);
//const Message = mongoose.model('Message', MessageSchema);
module.exports.User = User;
//module.exports.Chat = Chat;
//module.exports.Message = Message;