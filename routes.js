const express = require('express');
const router = express.Router();
const User = require('./model').User;
const Chat = require('./model').Chat;
const Message = require('./model').Message


/*router.get('/users/:name', (req, res) => {
    User.find({name:req.params.name}, (err, doc) => {
        if (err) return next(err);
        if (!doc) {
            err = new Error('User-Document not found');
            err.status = 404;
            return next(err);
        }
        res.json(doc);
        req.user = doc;
        return doc;
    });
});*/

router.get('/users/:id', (req, res) => {
    User.findById(req.params.id, (err, doc) => {
        if (err) return next(err);
        if (!doc) {
            err = new Error('User-Document not found');
            err.status = 404;
            return next(err);
        }
        res.json(doc);
        req.user = doc;
        return doc;
    });
});

router.get('/users', (req, res, next) => {
    User.find({}).sort({ createdAt: -1 }).exec((err, users) => {
        if (err) return next(err);
        res.json(users);
    });
});


//need two callbacks for two params?
//howto error handle properly so api doesn't crash
//how to use addContact fn defined in models? req.user.addContact doesn't work?

/*router.param('userID', (req, res, next, id) => {
    User.findById(id, (err, doc) => {
        if (err) return next(err);
        if (!doc) {
          err = new Error('User-Document not found');
          err.status = 404;
          return next(err);
        }
        req.user = doc;
        return next();
      });
});

router.param('friendID', (req, res, next, id) => {
    User.findById(id, (err, doc) => {
        if (err) return next(err);
        if (!doc) {
          err = new Error('User-Document not found');
          err.status = 404;
          return next(err);
        }
        req.friend = doc;
        return next();
      });
});*/

router.put('/users/addContact/', (req, res, next) => {
    User.findById(req.query.userID, (err, user) => {
        if (err) return next(err);
        if (!user) {
            err = new Error('User-Document not found');
            err.status = 404;
            return next(err);
        }
        req.user = user;
        req.user.contacts.push(req.query.friendID);
        console.log(req.user);
        User.findOneAndUpdate(req.query.userID, '-__v', req.user, {upsert:true}, (err, userToAddContact) => {
            if (err) return next(err);
            if (!userToAddContact) {
                err = new Error('User-Document not found');
                err.status = 404;
                return next(err);
            }
            res.json(req.user);
        });
    });
});

router.put('/users/removeContact/', (req, res, next) => {
    User.findById(req.query.friendId).exec((err, friend) => {
        if (err) return next(err);
        if (!friend) {  
            err = new Error('Friend-Document not found');
            err.status = 404;
            return next(err);
        }
        User.findById(req.query.userId).exec((err, user) => {
            if (err) return next(err);
            if (!user) {
                err = new Error('User-Document not found');
                err.status = 404;
                return next(err);
            }
            for(let i = 0; i < this.contacts.length; i++){
                if(this.contacts[i] == friend._id){
                  this.contacts.splice(i, 1, {});
                }
              }
            user.save();
            res.json(user)
        });
    });
});


///:userName/addContact/:friendName
//http://localhost:3000/users
//http://localhost:3000/users?name=Isaac&email=iqiao2011@gmail.com&password=foobar
router.post('/*', (req, res, next) => {

    
    const user = new User(req.body);

    user.name = req.query.name;
    user.email = req.query.email;
    user.password = req.query.password;

    //need a check for an undefined/param not included?
    //need to check for name or email collision
    user.save((err, user) => {
        /*User.findById(user.name, (err, doc) => {
            if (err) return next(err);
            if (!doc) {
                err = new Error('Document not found');
                err.status = 404;
                return next(err);
            }
            req.user = doc;
            return next();
        });

        User.findById(user.email, (err, doc) => {
            if (err) return next(err);
            if (!doc) {
                err = new Error('Document not found');
                err.status = 404;
                return next(err);
            }
            req.user = doc;
            return next();
        });*/
        console.log(user);
        if (err) return next(err);
        res.status(201);
        res.json(user);
    });
 });

router.delete('/users', (req, res) =>{
    User.remove({name: req.query.userName}, (err, user) => {
        if(err) return next(err);
        res.json(user);
    });
});

 module.exports = router;